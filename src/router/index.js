import Vue from "vue";
import VueRouter from "vue-router";
import Shorten from "../views/Shorten.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "shorten-url",
    component: Shorten
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
